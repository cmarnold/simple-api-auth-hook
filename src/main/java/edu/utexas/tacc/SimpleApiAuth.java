/**
 *
 */
package edu.utexas.tacc;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.auth.Authenticator;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;

import static org.apache.http.conn.ssl.SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;

/**
 * @author mrhanlon
 *
 */
public class SimpleApiAuth implements Authenticator {

  private static final Log _log = LogFactoryUtil.getLog(SimpleApiAuth.class);

  @Override
  public int authenticateByEmailAddress(long companyId, String emailAddress,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    _log.error("Authentication by email address not allowed");
    return Authenticator.FAILURE;
  }

  @Override
  public int authenticateByScreenName(long companyId, String screenName,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    return authenticate(companyId, screenName, password);
  }

  @Override
  public int authenticateByUserId(long companyId, long userId,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    _log.error("Authentication by user ID not allowed");
    return Authenticator.FAILURE;
  }
/** Updating this function to force tlsv1.2
  public static int authenticate(long companyId, String username, String password) {
    int authResponse = Authenticator.DNE;

    try {
      Executor exec = Executor.newInstance()
        .auth(new HttpHost(apiHost()), username, password);

      String jsonResp = exec.execute(Request.Post(apiBaseURI() + "/tokens/v1"))
        .returnContent().asString();
      _log.info("Auth response: " + jsonResp);

       JsonNode node = new ObjectMapper().readTree(jsonResp);
       String status = node.get("status").asText();
       if (status.toLowerCase().equals("success")) {
         JsonNode token = node.get("result").get(0).get("token");
         if (token != null) {
           return Authenticator.SUCCESS;
         }
       }
    } catch (Exception e) {
      _log.error(e);
    }

    return authResponse;
  }
**/

public static int authenticate(long companyId, String username, String password) {
  int authResponse = Authenticator.DNE;
  HttpPost method = null;

  try {
    SSLContext sslContext = SSLContexts.custom().useTLS().build();
    SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[] {"TLSv1.2"}, null, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);

    HttpClientBuilder clientBuilder = HttpClients.custom().setSSLSocketFactory(f);
    CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    Credentials defaultcreds = new UsernamePasswordCredentials(username, password);
    credentialsProvider.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM), defaultcreds);
    clientBuilder.setDefaultCredentialsProvider(credentialsProvider);
    CloseableHttpClient client = clientBuilder.build();

    method = new HttpPost(apiBaseURI() + "/tokens/v1");
      CloseableHttpResponse statusCode = client.execute(method);

      if (statusCode.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
        System.out.println("Method failed: " + statusCode.getStatusLine());
      }
      String jsonResp = EntityUtils.toString(statusCode.getEntity());

      System.out.println("Auth response: " + jsonResp);

      JsonNode node = new ObjectMapper().readTree(jsonResp);
      String status = node.get("status").asText();
      if (status.toLowerCase().equals("success")) {
        JsonNode token = node.get("result").get(0).get("token");
        if (token != null) {
          return Authenticator.SUCCESS;
        }
      }

    } catch (IOException e) {
    System.out.println("IOException " + e.getMessage());
    } catch (NoSuchAlgorithmException e) {
    System.out.println("NoSuchAlgo " + e.getMessage());
  } catch (KeyManagementException e) {
    System.out.println("KeyMgmnt " + e.getMessage());
  }  finally {
      method.releaseConnection();
    }

  return authResponse;
}

  private static String apiHost() {
    ResourceBundle bundle = ResourceBundle.getBundle("api");
    return bundle.getString("xsede.api.host");
  }

  private static String apiBaseURI() {
    ResourceBundle bundle = ResourceBundle.getBundle("api");
    String apiBase = bundle.getString("xsede.api.scheme")
        + bundle.getString("xsede.api.host") + ":"
        + bundle.getString("xsede.api.port")
        + bundle.getString("xsede.api.root");
    return apiBase;
  }

}
